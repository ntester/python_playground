import pprint

def setup_cvs_dictionary(cvs_string):

    # Get the data into a data structure we can parse
    # Splitting on \n to represent lines
    cvs_data = cvs_string.split('\n')

    # get column names from the cvs string
    header = cvs_data[0].split(",")
    cvs_data_arr = []

    for row in cvs_data[1:]:  # This is looping line by line like a cvs file
        values = row.split(',')
        data_dict = {column: [] for column in header}
        for column, value in zip(header, values):
            data_dict[column] = value

        cvs_data_arr.append(data_dict)
    return cvs_data_arr

def normalize_cvs_data(s_data, g_data):
    # We need to return a parent child data structure where id is same.
    # s_data will be parent data, and g_data with same id
    #
    normalize_data = []
    for s_dict in s_data:
        s_dict['child'] = []
        id_to_check = s_dict['S_Column_1_Id']
        # check if id is in other data structure
        for g_dict in g_data:
            if g_dict['G_Column_1_Id'] == id_to_check:
                s_dict['child'].append(g_dict)
        normalize_data.append(s_dict)

    return normalize_data

def normalize_cvs_data_optomized(s_data, g_data):
    # Create a dictionary to store all the dictionaries with the same G_Column_1_Id
    g_data_dict = {}
    for g_dict in g_data:
        id_to_check = g_dict['G_Column_1_Id']
        if id_to_check not in g_data_dict:
            g_data_dict[id_to_check] = []
        g_data_dict[id_to_check].append(g_dict)

    # Create the parent-child data structure
    normalize_data = []
    for s_dict in s_data:
        id_to_check = s_dict['S_Column_1_Id']
        child_list = g_data_dict.get(id_to_check, [])
        s_dict['child'] = child_list
        normalize_data.append(s_dict)

    return normalize_data



def parse_cvs_data(s_str, g_str):
    # Get our data structures first
    s_data = setup_cvs_dictionary(s_str)
    g_data = setup_cvs_dictionary(g_str)
    #normalized_data = normalize_cvs_data(s_data, g_data)
    normalized_data = normalize_cvs_data_optomized(s_data, g_data)

    return normalized_data



# Tests
import unittest

class ColumnTest(unittest.TestCase):
    def setUp(self):
        self.s_string = ''
        self.g_string = ''

    def test_normal_column_order(self):
        self.s_string = "S_Column_1_Id,S_Colomn_2,S_Colunm_3,S_Colunm_4,S_Colunm_5,S_Colunm_6,S_Colunm_7,S_Colunm_8\n5556767,column_2_value,column_3_value,column_4_value,column_5_value,column_6_value,column_7_value,column_8_value"

        self.g_string = ''
        cvs_data = setup_cvs_dictionary(self.s_string)
        print("\nMixed Column Data values: ")
        pprint.pprint(cvs_data)
        self.assertEqual(cvs_data[0]['S_Colomn_2'], 'column_2_value')

    def test_mixed_column_order(self):
        self.s_string = "S_Column_1_Id,S_Colunm_6,S_Colunm_3,S_Colunm_5,S_Colunm_4,S_Colunm_7,S_Colomn_2,S_Colunm_8\n5556767,column_6_value,column_3_value,column_5_value,column_4_value,column_7_value,column_2_value,column_8_value"
        cvs_data = setup_cvs_dictionary(self.s_string)
        print("\nData values: ")
        pprint.pprint(cvs_data)
        self.assertEqual(cvs_data[0]['S_Colomn_2'], 'column_2_value')

    def test_mixed_column_order(self):
        self.s_string = "S_Column_1_Id,S_Colunm_6,S_Colunm_3,S_Colunm_5,S_Colunm_4,S_Colunm_7,S_Colomn_2,S_Colunm_8\n5556767,column_6_value,column_3_value,column_5_value,column_4_value,column_7_value,column_2_value,column_8_value"
        cvs_data = setup_cvs_dictionary(self.s_string)
        print("\nData values: ")
        pprint.pprint(cvs_data)
        self.assertEqual(cvs_data[0]['S_Colomn_2'], 'column_2_value')

    def test_normalize(self):
        self.s_string = "S_Column_1_Id,S_Colunm_6,S_Colunm_3,S_Colunm_5,S_Colunm_4,S_Colunm_7,S_Colomn_2,S_Colunm_8\n5556767,column_6_value,column_3_value,column_5_value,column_4_value,column_7_value,column_2_value,column_8_value\n5556768,column_6_value_b,column_3_value_b,column_5_value_b,column_4_value_b,column_7_value_b,column_2_value_b,column_8_value_b"
        self.g_string = "G_Column_1_Id,G_Colunm_6,G_Colunm_3,G_Colunm_5,G_Colunm_4,G_Colunm_7,G_Colomn_2,G_Colunm_8\n5556767,g_column_6_value,g_column_3_value,g_column_5_value,g_column_4_value,g_column_7_value,g_column_2_value,g_column_8_value\n5556768,g_column_6_value_b,g_column_3_value_b,g_column_5_value_b,g_column_4_value_b,g_column_7_value_b,g_column_2_value_b,g_column_8_value_b\n5556767,g_column_6_value_c,g_column_3_value_c,g_column_5_value_c,g_column_4_value_c,g_column_7_value_c,g_column_2_value_c,g_column_8_value_c"

        #Get our data structures first
        s_data = setup_cvs_dictionary(self.s_string)
        g_data = setup_cvs_dictionary(self.g_string)


        #normalize_data = normalize_cvs_data(s_data, g_data)
        normalize_data = normalize_cvs_data_optomized(s_data, g_data)

        print("\nNormalize Data values: ")
        pprint.pprint(normalize_data)
        self.assertEqual(normalize_data[0]['S_Colomn_2'], 'column_2_value')
        self.assertEqual(normalize_data[0]['child'][0]['G_Colomn_2'], 'g_column_2_value')