import argparse
from typing import List

class BSTNode:
    def __init__(self, data, left,right):
        self.value = data
        self.left = left
        self.right = right

def breadth_search():
    # Create the tree to search
    root = BSTNode(
        9,
        BSTNode(
            5,
            BSTNode(3, None, None),
            BSTNode(6, None, None)),
        BSTNode(
            10,
            None,
            BSTNode(11, None, None)),
    )
    print(traverse(root))

    return(getBreadthSearchOrder(root))

def traverse(rootnode):
    thislevel = [rootnode]
    print_list = []
    while thislevel:
        nextlevel = list()
        for node in thislevel:
            print_list.append(node.value)
            if node.left:
                nextlevel.append(node.left)
            if node.right:
                nextlevel.append(node.right)

        thislevel = nextlevel

    return print_list

def getBreadthSearchOrder(root, result = []):

    if root:
        getBreadthSearchOrder(root.left, result)
        result.append(root.value)
        getBreadthSearchOrder(root.right, result)

    return result

def rotate_matrix_90(matrix):
    print("the rotate_matrix_90 method returns: ")
    print(f"before reverse {matrix}")
    matrix.reverse()
    print(f"after reverse {matrix}")
    for i in range(len(matrix)):
        for j in range(i):
            # swap (which is the transpose)
            matrix[i][j], matrix[j][i] = matrix[j][i], matrix[i][j]
    return matrix

def roman_to_int():
    roman_dict = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
    roman_str = "MCMXCIV"
    results = 0
    for i in range(len(roman_str)):
        if i +1 < len(roman_str) and roman_dict[roman_str[i]] <  roman_dict[roman_str[i + 1]] :
            results -= roman_dict[roman_str[i]]
        else:
            results += roman_dict[roman_str[i]]

    return results

def bubble_sort():
    print("The bubble_sort method returns: ")
    list_int = [10, 2, 13, 4, 5, 46]
    print(f"The original list is: {list_int}")
    for i in range(len(list_int)):
        for j in range(len(list_int) -1):
            if list_int[j] > list_int[j+1]:
                # we swap for the sorting
                list_int[j], list_int[j+1] = list_int[j+1],list_int[j]
    return(list_int)

def binary_search():
    print("The binary_search method returns: ")
    list_ints = [-1, 0, 3, 5, 9, 12]
    target = 9

    if target == list_ints[0]:
        return 0
    start = 0
    end = len(list_ints)
    midpoint = end//2

    while(start < midpoint and end > midpoint):

        if target > list_ints[midpoint]:
            start = midpoint
            midpoint = (end + start)//2
        elif target < list_ints[midpoint]:
            end = midpoint
            midpoint = (end + start)//2
        elif target == list_ints[midpoint]:
            return midpoint
    return -1

def check_palindrom():
    # strings that are palindromes
    palindrome_str = 'civic'
    print(f"is this string a palindrome [ {palindrome_str} ]")
    return is_palindrom(palindrome_str.lower())

def is_palindrom(string):
    left, right = 0, len(string) -1

    while left < right:
        if string[left] != string[right]:
            return False
        left += 1
        right -= 1
    return True

"""
You are given an array of integers. Find the largest subarray sum.
A subarray is a contiguous non-empty sequence of elements within an array.
The given array will have at least one positive or zero number.

Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
Output: 6
Explanation: The subarray [4,-1,2,1] has the largest sum 6.

Input: nums = [5,4,-1,7,8,-2]
[5,-4,-1,-7,-8,-2]
Output: 23
Explanation: The subarray [5,4,-1,7,8] has the largest sum 23.
"""
def get_max_sum_sub_array():
    # Use a breakpoint in the code line below to debug your script.
    list_int = [5, 4, -1, 7, 8, -2]
    print(f'List is:, {list_int}')  # Press ⌘F8 to toggle the breakpoint.
    largest_num = float('-inf')  # Initialize with negative infinity
    cur_sum = 0
    for num in list_int:
        #Lets add the number and check if it is largest num
        cur_sum += num
        if cur_sum > largest_num:
            largest_num = cur_sum
        # We do not need to keep the negative number in our current sum
        if cur_sum < 0:
            cur_sum = 0

    return largest_num

# Here is an example how to use typing.
def two_sums(numbers: List[int], target: int) -> List[int]:
    # Given an array of integers nums and an integer target,
    # return indices of the two numbers such that they add up to target.
    # Use a dictionary to keep the subtracted value where dictionary
    # key is number and value is the index
    # The first pass will always get put on the prev_values dictionary.
    prev_values = {}
    print(f'The numbers list is {numbers} and the target is {target}')
    for i, num in enumerate(numbers):
        if target - num in prev_values:
            print(f'we return the 2 indexes where the values added equal the target')
            return [prev_values[target - num], i]
        else:
            prev_values[num] = i

methods = {
    'rotate_matrix_90': rotate_matrix_90,
    'binary_search': binary_search,
    'bubble_sort': bubble_sort,
    'breadth_search': breadth_search,
    'roman_to_int': roman_to_int,
    'check_palindrom': check_palindrom,
    'get_max_sum_sub_array': get_max_sum_sub_array
}
method_list = list(methods.keys())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Perform Leetcode style methods.', usage=methods.keys())
    parser.add_argument('method', nargs='?', choices=methods.keys(), help='Select from this list of methods that can be performed')
    args = parser.parse_args()
    if args.method:
        print(methods[args.method]())
    else:
        print('No method was supplied so we use the default two_sums ')
        print(f'If you want to use a different method pick one from this list: \n{method_list}\n')

        print(two_sums([1, 2, 3, 5], 4))
